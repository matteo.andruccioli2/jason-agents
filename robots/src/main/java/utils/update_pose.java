package utils;

import env.Direction;
import env.Vector2D;
import jason.NoValueException;
import jason.asSemantics.Agent;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.Atom;
import jason.asSyntax.Literal;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.Term;

public class update_pose extends DefaultInternalAction {

    private enum Facing {
        TOP(1, 0),
        RIGHT(0, 1),
        BOTTOM(-1, 0),
        LEFT(0, -1);

        Facing(int x, int y) {
            this.x = x;
            this.y = y;
        }

        static Facing fromLiteral(Literal literal) {
            if (!literal.getTerm(0).isAtom()) {
                throw new IllegalArgumentException("Cannot parse as Facing: " + literal);
            }
            return valueOf(((Atom) literal.getTerm(0)).getFunctor().toUpperCase());
        }

        private final int x, y;

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public Facing rotate(Direction direction) {
            return values()[direction.ordinal() / 2 % 4];
        }

        public Vector2D asVector() {
            return Vector2D.of(x, y);
        }
    }

    private static Vector2D positionToVector(Literal literal) {
        if (!literal.getTerm(0).isNumeric() || !literal.getTerm(1).isNumeric()) {
            throw new IllegalArgumentException("Cannot parse as Vector2D: " + literal);
        }
        try {
            return Vector2D.of(
                    (int) ((NumberTerm)literal.getTerm(0)).solve(),
                    (int) ((NumberTerm)literal.getTerm(1)).solve()
            );
        } catch (NoValueException e) {
            throw new IllegalArgumentException("Cannot parse as Vector2D: " + literal);
        }
    }

    private static Direction directionFromTerm(Term term) {
        if (!term.isAtom()) {
            throw new IllegalArgumentException("Cannot parse as Direction: " + term);
        }
        return Direction.valueOf(((Atom) term).getFunctor().toUpperCase());
    }

    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
        Agent currentAgent = ts.getAg();
        Literal currentPosition = currentAgent.findBel(Literal.parseLiteral("position(X, Y)"), un);
        Literal currentFacing = currentAgent.findBel(Literal.parseLiteral("facing(Dir)"), un);
        Term nextDirection = args[0];
        currentAgent.delBel(currentFacing);
        currentAgent.delBel(currentPosition);

        Vector2D position = positionToVector(currentPosition);
        Facing facing = Facing.fromLiteral(currentFacing);
        Direction dir = directionFromTerm(nextDirection);

        Facing nextFacing = facing.rotate(dir);
        Vector2D nextPosition = position.plus(nextFacing.asVector());

        currentAgent.addBel(Literal.parseLiteral("position" + nextPosition));
        currentAgent.addBel(Literal.parseLiteral("facing(" + nextFacing.toString().toLowerCase() + ")"));
        return true;
    }
}
